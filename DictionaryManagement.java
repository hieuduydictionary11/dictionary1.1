package dictionary;

/**
 * DictionaryManagement la chuong trinh gom cac thao tac nhap tu ban phim, nhap tu file, tim tu
 * @author Trinh Duc Duy, Le Van Trung Hieu
 * @version 1.2
 * @since 2018-10-07
 */

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;

public class DictionaryManagement {
    
    /**
    * insertFromCommandline la 1 phuong thuc nhap du lieu tu va giai thich
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
	 
    public void insertFromCommandline(ArrayList<Word> dict) {
        
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap so luong tu: "); //in ra yeu cau nhap so luong tu
        int n = scan.nextInt(); //nhap so luong tu
        scan.nextLine();
        
        for(int i=0;i<n;i++) {
            
            System.out.print("Nhap tu tieng Anh: "); //in ra yeu cau nhap tu tieng Anh
            String target = scan.next(); //nhap tu tieng Anh
            scan.nextLine();
            System.out.print("Nhap giai thich tieng Viet: "); //in ra yeu cau nhap giai thich tieng Viet
            String explain = scan.nextLine(); //nhap giai thich
            
            Word newWord = new Word(target, explain); //khoi tao doi tuong lop Word voi hai thuoc tinh nhap o tren
            
            dict.add(newWord); //them doi tuong vua tao vao mang
        }
        
    }
    
    /**
    * insertFromFile la 1 phuong thuc nhap du lieu tu file "dictionaries.txt"
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
	
    public void insertFromFile(ArrayList<Word> dict) {
        
        File file = new File("dictionaries.txt"); //doc file "dictionaries.txt"
        
        try(Scanner scan = new Scanner(file)) {
            
            String target; //khoi tao bien luu tu tieng Anh
            String explain; //khoi tao bien luu tu tieng Viet
            
            while(scan.hasNext()) {
                
                target = scan.next(); //nhap tu file tu tieng Anh
                explain = scan.nextLine(); //nhap tu file tu tieng Viet
                
                Word newWord = new Word(target, explain.trim()); //khoi tao doi tuong lop Word voi hai thuoc tinh nhap o tren
                
                dict.add(newWord); //them doi tuong vua tao vao mang
            }
            
	} catch(Exception e) {
            
	}
        
    }
    
    /**
    * dictionaryLookup la 1 phuong thuc tim kiem tu tieng Anh
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
    
    public void dictionaryLookup(ArrayList<Word> dict) {
        
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Nhap tu tieng Anh can tim: "); //in yeu cau nhap tu can tim kiem
        String targetInput; //khoi tao biem luu tu can tim
        targetInput = scan.next(); //nhap tu can tim
        
        /*
        Tim kiem nghia cua tu va in ra man hinh
        */
        
        System.out.print("Nghia cua tu: "); 
        for (Word dict1 : dict) {
            
            if(targetInput.equalsIgnoreCase(dict1.getTarget())) {
                
                System.out.println(dict1.getExplain());
                break;
                
            }
            
        }
        
    }
    
}
