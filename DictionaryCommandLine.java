package dictionary;

/**
 * DictionaryCommandLine la chuong trinh in toan bo cac tu trong cmd, tra tu
 * @author Trinh Duc Duy, Le Van Trung Hieu
 * @version 1.1
 * @since 2018-10-07
 */

import java.util.ArrayList;
import java.util.Scanner;

public class DictionaryCommandLine {
    
    /**
    * showAllWords la 1 phuong thuc in ra toan bo tu trong mang
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
    
    public void showAllWords(ArrayList<Word> dict) {
        
        /*
        Tinh chinh viec in tu ra man hinh
        Phu hop voi so luong khoang 1 den 999.999 tu
        Phu hop voi tu tieng Anh khong qua 17 chu cai
        */
        
        System.out.println("No"+"     "+"|English"+"          "+"|Vietnamese");
        for(int i=0;i<dict.size();i++) {
            
            System.out.print((i+1));
            
            String s;
            s = String.valueOf((i+1));
            
            for(int j=0;j<(7-s.length());j++) {
                System.out.print(" ");
            }
            
            System.out.print("|"+dict.get(i).getTarget());
            
            for(int k=0;k<(17-dict.get(i).getTarget().length());k++) {
                System.out.print(" ");
            }
            
            System.out.println("|"+dict.get(i).getExplain());  
        }
        
        System.out.println("");
    }
    
    /**
    * dictionaryBasic la 1 phuong thuc nhap tu va in tu
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
    
    public void dictionaryBasic(ArrayList<Word> dict) {
        
        DictionaryManagement word = new DictionaryManagement(); //khoi tao doi tuong lop DictionaryManagement
        
        word.insertFromCommandline(dict); //chay ham insertFromCommandline tu lop DictionaryManagement
        
        showAllWords(dict); //in ra toan bo cac tu cua mang dict
        
    }
    
    /**
    * dictionaryAdvanced la 1 phuong thuc nhap tu file,in ra man hinh va tra tu
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
    
    public void dictionaryAdvanced(ArrayList<Word> dict) {
        
        DictionaryManagement word = new DictionaryManagement(); //khoi tao doi tuong lop DictionaryManagement
        
        word.insertFromFile(dict); //chay ham insertFromFile tu lop DictionaryManagement
        
        showAllWords(dict); //in ra toan bo cac tu cua mang dict
        
        word.dictionaryLookup(dict); //tim kiem tu tieng Anh
    }
    
    /**
    * dictionarySeacher la 1 phuong thuc tra tu
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
    
    public void dictionarySeacher(ArrayList<Word> dict) {
        
        Scanner scan = new Scanner(System.in);
        ArrayList<Word> searcher = new ArrayList<>(); //khoi tao mang luu cac tu tra duoc
        
        System.out.print("Nhap tu can tra: "); //in ra yeu cau nhap tu
        
        String search = scan.next();; //nhap vao tu can tra
        
        boolean check = false; //tao bien kiem tra viec tim duoc tu
        
        for (Word dict1 : dict) {
            /*
             kiem tra viec tra duoc tu
            */
            if(dict1.getTarget().startsWith(search)) {
                
                searcher.add(dict1);
                
                check = true;
                
            } 
            
        }
        
        if(check==true) {
            
            showAllWords(searcher); //in ra cac tu tra duoc
            
        } else {
            System.out.println("Word not found!"); //thong bao neu khong tim duoc tu
        }
        
    }
    
}

